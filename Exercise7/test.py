import gwpy

from gwosc.datasets import event_gps
gps = event_gps('GW150914')
print(gps)

segment = (int(gps), int(gps)+1)

from gwpy.timeseries import TimeSeries
data = TimeSeries.fetch_open_data('L1', *segment, verbose=True)
print(data)
